//////////////1

db.users.insertMany([{
	firstName : "Noelle",
	lastName : "Silva",
	email : "nsilva@gmail.com",
	password : "12345a",
	isAdmin : false
},
{
	firstName : "Charmie",
	lastName : "Papitson",
	email : "charms@gmail.com",
	password : "12345b",
	isAdmin : false
},
{
	firstName : "Yami",
	lastName : "Sukehiro",
	email : "yamisukehiro@gmail.com",
	password : "12345c",
	isAdmin : false
},
{
	firstName : "Gordon",
	lastName : "Agrippa",
	email : "gordon@gmail.com",
	password : "12345d",
	isAdmin : false
},
{
	firstName : "Luck",
	lastName : "Voltia",
	email : "luckthunder@gmail.com",
	password : "12345e",
	isAdmin : false
}])


///////////////2

db.courses.insertMany([{
	name : "IT101",
	price : "1000",
	isActive : false
},
{
	name : "DBMS101",
	price : "2000",
	isActive : false
},
{
	name : "ITEthics101",
	price : "550",
	isActive : false
}
])

///////////3

db.users.find({isAdmin:false})

//////////////4

db.users.updateOne({},{
	$set: {
		isAdmin : true
	}
})

/////////////5

db.courses.updateOne({name : "ITEthics101"},{
	$set:{
		isActive : true
	}
})

////////////////6

db.courses.deleteMany({isActive : false})
/*
	Activity:

	>> Using Robo3T, repopulate our users collections using CRUD operations (Same database: session28)

//Create
	>> Add at least five of the members of your favorite fictional group or musical band into our users collection with the following fields:

		-firstName -string
		-lastName -string
		-email -string
		-password -string
		-isAdmin -boolean 
			note: initially none of the users are admin.

	>> Add 3 new courses in a new courses collection with the following fields:

		-name - string
		-price - number
		-isActive - boolean 
			note: initially all courses should be inactive

	>> Note: If the collection you are trying to add a document to does not exist yet, mongodb will add it for you. 

//Read

	>> Find all regular/non-admin users.

//Update

	>> Update the first user in the users collection as an admin.
	>> Update one of the courses as active.

//Delete

	>> Delete all inactive courses


//Add all of your query/commands here in activity.js

*/